package net.powerpiston.pong.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import net.powerpiston.pong.MainGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width=960;
        config.height=540;
        config.fullscreen = false;
		new LwjglApplication(new MainGame(), config);
	}
}
