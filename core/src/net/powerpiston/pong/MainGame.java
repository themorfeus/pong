package net.powerpiston.pong;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.powerpiston.pong.screens.MenuScreen;
import net.powerpiston.pong.shapes.Point;

import java.nio.ByteBuffer;
import java.util.HashMap;

public class MainGame extends Game {

    public static final float VIRTUAL_WIDTH = 1920;
    public static final float VIRTUAL_HEIGHT = 1080;

    public static float MOUSE_SENSITIVITY = 2f;

    public static final String FONT_FILE = "visitor2.ttf";

    public ShapeRenderer shaper;
    public SpriteBatch batch;
    public Camera cam;
    public Stage stage;

    //Game and Stage viewports
    public Viewport gv;
    public Viewport sv;

    private static boolean mobile = false;

    private static HashMap<Integer, BitmapFont> fonts;


    private Matrix4 renderMatrix;
    private FrameBuffer fbo;
    private Mesh screenQuad;
    private ShaderProgram shader;

    private float tX, tY;

    public MainGame(){this(false); }
    public MainGame(boolean mob){mobile = mob; }

    private boolean crtShader =true;

    private float alpha = 0;
    private float chroma  = 0;
    private boolean cursorGrabbed = false;

	@Override
	public void create () {

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        //CAN be perspective camera, theoretically
        cam = new OrthographicCamera();
        batch = new SpriteBatch();
        shaper = new ShapeRenderer();
        stage = new Stage();
        cam = new OrthographicCamera();

        //Shader stuff
        renderMatrix = new Matrix4();
        fbo = new FrameBuffer(Pixmap.Format.RGB565, (int)VIRTUAL_WIDTH, (int)VIRTUAL_HEIGHT, false);
        screenQuad = createFullScreenQuad();

        String vertexShader = Gdx.files.internal("shaders/crt.v").readString();
        String fragmentShader = Gdx.files.internal("shaders/crt.f").readString();

        shader = new ShaderProgram(vertexShader, fragmentShader);

        fonts = new HashMap<Integer, BitmapFont>();

        cam.position.set(0,0,0);

        stage.setViewport(new FitViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT));

        gv = new FitViewport(VIRTUAL_WIDTH,VIRTUAL_HEIGHT, cam);
        sv = stage.getViewport();

        gv.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
        sv.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);

        Gdx.input.setCursorCatched(true);

        tX = this.getAbsTouchPoint().getX();
        tY = this.getAbsTouchPoint().getY();

        this.setScreen(new MenuScreen(this));
	}


	@Override
	public void render () {

//        if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
//            cursorGrabbed=!cursorGrabbed;
//            Gdx.input.setCursorCatched(cursorGrabbed);
//        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.F2)){
            takeScreenshot();
        }

//        if(Gdx.input.isKeyJustPressed(Input.Keys.F1)){
//            crtShader = !crtShader;
//
//            String vertexShader, fragmentShader;
//
//            if(crtShader){
//                vertexShader = Gdx.files.internal("shaders/crt.v").readString();
//                fragmentShader = Gdx.files.internal("shaders/crt.f").readString();
//            }else{
//                vertexShader = Gdx.files.internal("shaders/passthrough.v").readString();
//                fragmentShader = Gdx.files.internal("shaders/passthrough.f").readString();
//            }
//
//            shader = new ShaderProgram(vertexShader, fragmentShader);
//        }

        tX+=Gdx.input.getDeltaX() * MOUSE_SENSITIVITY;
        tY-=Gdx.input.getDeltaY() * MOUSE_SENSITIVITY;

        if(tX<0)tX = 0; if(tX>MainGame.VIRTUAL_WIDTH)tX = MainGame.VIRTUAL_WIDTH;
        if(tY<0)tY = 0; if(tY>MainGame.VIRTUAL_HEIGHT)tY = MainGame.VIRTUAL_HEIGHT;


        fbo.begin();
            Gdx.gl.glClearColor(0,0,0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            batch.setProjectionMatrix(cam.combined);
            shaper.setProjectionMatrix(cam.combined);
            batch.begin();
            super.render();
            batch.end();
        fbo.end();

        Gdx.gl.glClearColor(0,0,0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl20.glEnable(GL20.GL_TEXTURE_2D);



        alpha+=0.05f;
        chroma = (float)Math.sin(alpha)/8;

        if(chroma<0)chroma = 0;

        cam.update();

        //Apply game viewport(s) before rendering
        gv.apply();

        fbo.getColorBufferTexture().bind();
        shader.begin();
        {
            shader.setUniformMatrix("u_projView", renderMatrix);
            shader.setUniformi("u_texture", 0);

            //Values for CRT shader
            if(crtShader) {
                shader.setUniformf("Distortion", .2f);
                shader.setUniformf("zoom", 1f);
                shader.setUniformf("tint", 1f, 1f, .85f);
                shader.setUniformf("time", alpha);
                shader.setUniformf("chromaticDispersion", .1f, .1f);


                //Pixelate shader values
//                shader.setUniformf("vx_offset", 100);
//
//                shader.setUniformf("rt_w", MainGame.VIRTUAL_WIDTH);
//                shader.setUniformf("rt_h", MainGame.VIRTUAL_HEIGHT);
//                shader.setUniformf("pixel_w", 20);
//                shader.setUniformf("pixel_h", 20);

            }

            screenQuad.render(shader, GL20.GL_TRIANGLE_FAN);
        }shader.end();

        //Apply GUI (stage) viewport begore rendering
        sv.apply();
        stage.act();
        stage.draw();
    }


    @Override
    public void resize(int width, int height){
        super.resize(width, height);
        gv.update(width, height);
        sv.update(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        batch.dispose();
    }

    @Override
    public void pause() { super.pause(); }

    @Override
    public void resume() { super.resume(); }


    public Point getGameTouchPoint(){
        return new Point(tX, tY);
    }

    public void calibrateTouch(){
        this.tX = MainGame.VIRTUAL_WIDTH/2;
        this.tY = MainGame.VIRTUAL_HEIGHT/2;
    }

    public Point getAbsTouchPoint(){
        float nx, ny;

        nx = gv.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).x;
        ny = gv.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).y;

        return new Point(nx, ny);
    }

    public Point getStageTouchPoint(){
        float nx, ny;

        nx = sv.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).x;
        ny = sv.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).y;

        return new Point(nx, ny);
    }


    public static BitmapFont getFont(int size){

        if(!fonts.containsKey(size)){
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(MainGame.FONT_FILE));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = size;

            fonts.put(size, generator.generateFont(parameter));
            generator.dispose();
        }

        return fonts.get(size);
    }

    private void takeScreenshot(){
        int counter = 0;
        try{
            FileHandle fh;
            do{
                fh = new FileHandle("screenshot" + counter++ + ".png");
            }while (fh.exists());
            Pixmap pixmap = getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
            PixmapIO.writePNG(fh, pixmap);
            pixmap.dispose();
            System.out.println("Saved screenshot at " + fh.file().getAbsolutePath());
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    private static Pixmap getScreenshot(int x, int y, int w, int h, boolean yDown){
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);

        if (yDown) {
            // Flip the pixmap upside down
            ByteBuffer pixels = pixmap.getPixels();
            int numBytes = w * h * 4;
            byte[] lines = new byte[numBytes];
            int numBytesPerLine = w * 4;
            for (int i = 0; i < h; i++) {
                pixels.position((h - i - 1) * numBytesPerLine);
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
            }
            pixels.clear();
            pixels.put(lines);
        }

        return pixmap;
    }

    public Mesh createFullScreenQuad() {

        float[] verts = new float[20];
        int i = 0;

        verts[i++] = -1; // x1
        verts[i++] = -1; // y1
        verts[i++] = 0;
        verts[i++] = 0f; // u1
        verts[i++] = 0f; // v1

        verts[i++] = 1f; // x2
        verts[i++] = -1; // y2
        verts[i++] = 0;
        verts[i++] = 1f; // u2
        verts[i++] = 0f; // v2

        verts[i++] = 1f; // x3
        verts[i++] = 1f; // y2
        verts[i++] = 0;
        verts[i++] = 1f; // u3
        verts[i++] = 1f; // v3

        verts[i++] = -1; // x4
        verts[i++] = 1f; // y4
        verts[i++] = 0;
        verts[i++] = 0f; // u4
        verts[i++] = 1f; // v4

        Mesh mesh = new Mesh( true, 4, 0,  // static mesh with 4 vertices and no indices
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

        mesh.setVertices( verts );
        return mesh;
    }
}
