package net.powerpiston.pong.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import net.powerpiston.pong.MainGame;
import net.powerpiston.pong.screens.GameScreen;



/**
 * Created by morfeus on 2015-03-21.
 */
public class BallTrail extends Entity{

    private GameScreen gs;

    private Vector2 pos;
    private float radius;
    public BallTrail(MainGame g, GameScreen gs, Vector2 pos, float radius) {
        super(g);
        this.gs = gs;
        this.pos = pos;
        this.radius = radius;
    }

    private float alpha = .6f;

    @Override
    public void render() {
        alpha-=.1f;

        g.batch.end();

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        g.shaper.begin(ShapeRenderer.ShapeType.Filled);

        g.shaper.setColor(new Color(1f, 1f, 1f, alpha));
        g.shaper.rect(pos.x-radius, pos.y-radius, radius * 2, radius * 2);

        g.shaper.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        g.batch.begin();
    }


    @Override
    public void update() {
        if(alpha<0)gs.removeEntity(this);

    }
}
