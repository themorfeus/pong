package net.powerpiston.pong.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import net.powerpiston.pong.MainGame;

import java.util.Random;


/**
 * Created by morfeus on 2015-03-21.
 */
public class Paddle extends Entity{

    protected Rectangle rect;
    protected Color c;

    protected Vector2 pos;

    private boolean horizontal;

    public Paddle(MainGame g, Rectangle rect, Color c, boolean horizontal){
        super(g);
        this.rect = rect;
        this.c = c;

        this.horizontal = horizontal;

        if(!horizontal)pos = new Vector2(0, MainGame.VIRTUAL_HEIGHT/2);
        else pos = new Vector2();
    }

    @Override
    public void render() {
        g.batch.end();
        g.shaper.begin(ShapeRenderer.ShapeType.Filled);

        g.shaper.setColor(c);
        g.shaper.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());

        g.shaper.end();
        g.batch.begin();
    }

    private float targetY = -1;
    private float targetX = -1;

    private float randSpeedOffs = 0;

    @Override
    public void update() {

        if(targetY==-1 && targetX==-1) {
            if (horizontal) {
                if (pos.y - rect.getHeight() / 2 < 25) pos.y = 25 + rect.getHeight() / 2;

                if (pos.y + rect.getHeight() / 2 > MainGame.VIRTUAL_HEIGHT - 25)
                    pos.y = MainGame.VIRTUAL_HEIGHT - 25 - rect.getHeight() / 2;

                rect.setY(pos.y - rect.getHeight() / 2);
            } else {
                if (pos.x - rect.getWidth() / 2 < 0) pos.x = rect.getWidth() / 2;

                if (pos.x + rect.getWidth() / 2 > MainGame.VIRTUAL_WIDTH)
                    pos.x = MainGame.VIRTUAL_WIDTH - rect.getWidth() / 2;

                rect.setX(pos.x - rect.getWidth() / 2);
            }
        }else{
            if (horizontal) {
                randSpeedOffs = new Random().nextInt(5) - 2;

                if (targetY < pos.y) pos.y -= (15 + randSpeedOffs);
                if (targetY > pos.y) pos.y += (15 + randSpeedOffs);

                if (Math.abs(targetY - pos.y) < 15) pos.y = targetY;

                if (pos.y - rect.getHeight() / 2 < 25) pos.y = 25 + rect.getHeight() / 2;

                if (pos.y + rect.getHeight() / 2 > MainGame.VIRTUAL_HEIGHT - 25)
                    pos.y = MainGame.VIRTUAL_HEIGHT - 25 - rect.getHeight() / 2;

                rect.setY(pos.y - rect.getHeight() / 2);
            } else {
                randSpeedOffs = new Random().nextInt(5) - 2;

                if (targetX < pos.x) pos.x -= (15 + randSpeedOffs);
                if (targetX > pos.x) pos.x += (15 + randSpeedOffs);

                if (Math.abs(targetX - pos.x) < 15) pos.y = targetY;

                if (pos.x - rect.getWidth() / 2 < 0) pos.x = rect.getWidth() / 2;

                if (pos.x + rect.getWidth() / 2 > MainGame.VIRTUAL_WIDTH)
                    pos.x = MainGame.VIRTUAL_WIDTH - rect.getWidth() / 2;

                rect.setX(pos.x - rect.getWidth() / 2);
            }
        }

    }

    public boolean isVertical(){
        return !horizontal;
    }


    public void setX(float x){
        this.pos.x = x;
    }
    public void setY(float y){
        this.pos.y = y;
    }

    public void moveTowardsY(float y){
        targetY = y;
    }

    public Rectangle getRect(){
        return rect;
    }
}
