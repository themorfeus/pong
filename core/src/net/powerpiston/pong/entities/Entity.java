package net.powerpiston.pong.entities;

import net.powerpiston.pong.MainGame;

/**
 * Created by morfeus on 2015-03-21.
 */
public abstract class Entity {

    protected MainGame g;

    public Entity(MainGame g){
        this.g = g;
    }


    public abstract void render();
    public abstract void update();
}
