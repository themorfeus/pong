package net.powerpiston.pong.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;

import net.powerpiston.pong.MainGame;
import net.powerpiston.pong.screens.GameScreen;

import java.util.Random;


/**
 * Created by morfeus on 2015-03-21.
 */
public class Ball extends Entity{

    private Circle c;
    private GameScreen gs;

    private Vector2 pos;

    private float angle = 135;

    private float speed = 2;

    private boolean moving = false;

    private Sound bounce = Gdx.audio.newSound(Gdx.files.internal("snd/bounce.wav"));

    public Ball(MainGame g, GameScreen gs, float angle, float speed) {
        this(g,gs);
        this.angle = angle;
        this.speed = speed;
    }

    public Ball(MainGame g, GameScreen gs) {
        super(g);
        this.gs = gs;
        c = new Circle(MainGame.VIRTUAL_WIDTH/2, MainGame.VIRTUAL_HEIGHT/2, 10);
        pos = new Vector2(MainGame.VIRTUAL_WIDTH/2, MainGame.VIRTUAL_HEIGHT/2);

        angle = 45  + (new Random().nextInt(4) * 90);
    }

    @Override
    public void render() {
        g.batch.end();
        g.shaper.begin(ShapeRenderer.ShapeType.Filled);

        g.shaper.setColor(Color.WHITE);
        g.shaper.circle(pos.x, pos.y, c.radius);
        g.shaper.rect(pos.x-c.radius, pos.y-c.radius, c.radius * 2, c.radius * 2);

        g.shaper.end();
        g.batch.begin();
    }

    private boolean inverted;

    private float vX = 15, vY = 15;

    @Override
    public void update() {


        if(moving)pos.x +=   speed * vX * Math.cos(angle * Math.PI / 180);
        if(moving)pos.y +=   speed * vY * Math.sin(angle * Math.PI / 180);


        if(pos.x < 0 || pos.y < 0)gs.endGame();

        if(pos.x > MainGame.VIRTUAL_WIDTH || pos.y > MainGame.VIRTUAL_HEIGHT)gs.addPoint();


        c.setX(pos.x);
        c.setY(pos.y);


        for(Entity e:gs.getEntities()){

            if(e instanceof Paddle){
                Paddle p = (Paddle)e;

//                if((c.getY()<p.getRect().getY()+p.getRect().getHeight() && c.getY() > p.getRect().getY())
//                        &&   ((p.getRect().getX() < c.getX() && Point.distance(p.getRect().getX() +p.getRect().getWidth(), c.getY(), c.getX(), c.getY()) < c.getRadius())
//                        || (p.getRect().getX() > c.getX() && Point.distance(p.getRect().getX(), c.getY(), c.getX(), c.getY()) < c.getRadius()))){
                Circle pC = new Circle((float)(c.x + vX * Math.cos(angle * Math.PI / 180)), (float)(c.y + vY * Math.sin(angle * Math.PI / 180)), c.radius);

                if(Intersector.overlaps(pC, p.getRect())){
                    System.out.println("boom");
                    bounce.play(.25f);
                    lagAmt = (10 + new Random().nextInt(10));
                    if(p.isVertical())vY*=-1;
                    else vX*=-1;
                }
            }

        }

        float offsX, offsY;

        offsX = 0;
        offsY = 0;

        gs.addEntity(new BallTrail(g, gs, new Vector2(pos.x + offsX, pos.y + offsY), c.radius));
    }

    public void setMoving(boolean m){
        moving = m;
    }

    public float getY(){
        return pos.y;
    }

    private float lagAmt = (10 + new Random().nextInt(5));

    public float getLaggedY(){
        return (float) (pos.y + speed * lagAmt * vY * Math.sin(angle * Math.PI / 180));
    }
    public float getX(){
        return pos.x;
    }
}
