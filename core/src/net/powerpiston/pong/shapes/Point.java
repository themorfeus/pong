package net.powerpiston.pong.shapes;

import com.badlogic.gdx.math.Shape2D;

/**
 * Created by morfeus on 2015-03-18.
 */
public class Point extends Shape implements Shape2D {

    private float x, y;

    public Point(float x, float y){
        this.x = x;
        this.y = y;
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public static float distance(Point i, Point j){
        return distance(i.getX(), i.getY(), j.getX(), j.getY());
    }

    public static float distance(float x1,float y1,float x2,float y2){

        float a = Math.abs(x2-x1);
        float b = Math.abs(y2-y1);

        return (float)Math.sqrt((a*a) + (b*b));
    }

    @Override
    public boolean intersects(Shape s) {
        return false;
    }

    @Override
    public Point[] getVertices() {
        return new Point[]{this};
    }

    @Override
    public boolean contains(Point p) {
        return false;
    }
}
