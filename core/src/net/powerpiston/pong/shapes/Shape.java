package net.powerpiston.pong.shapes;


public abstract class Shape{

    public abstract Point[] getVertices();
    public abstract boolean contains(Point p);
    public abstract boolean intersects(Shape s);

}
