package net.powerpiston.pong.shapes;

import com.badlogic.gdx.math.Shape2D;

/**
 * Created by morfeus on 2015-03-18.
 */
public class Triangle extends Shape implements Shape2D{

    private Point[] vertices;
    private float area;

    public Triangle(float x1, float y1, float x2, float y2, float x3, float y3){
        this(new Point(x1,y1), new Point(x2,y2), new Point(x3,y3));
    }

    public Triangle(Point a, Point b, Point c){

        vertices = new Point[3];

        vertices[0] = a;
        vertices[1] = b;
        vertices[2] = c;

        this.area = Triangle.calcTriArea(this);
    }


    public boolean contains(float x, float y){
        return contains(new Point(x,y));
    }


    public boolean intersects(Triangle t){



        return false;
    }

    @Override
    public boolean contains(Point p){
        //Calc the origin triangle, then calc three resulting triangles.

        double parentArea = this.getArea();

        Triangle a = new Triangle(this.getPoint(0), this.getPoint(1), p);
        Triangle b = new Triangle(this.getPoint(1), this.getPoint(2), p);
        Triangle c = new Triangle(this.getPoint(2), this.getPoint(0), p);

        double sum = a.getArea() + b.getArea() + c.getArea();

        if(sum>parentArea+1.1d){
            //System.out.println(sum-parentArea);
            return false;
        }

        return true;
    }

    @Override
    public boolean intersects(Shape s) {

        return false;
    }

    @Override
    public Point[] getVertices(){
        return vertices;
    }

    public Point getPoint(int index){
        if(index<0 || index>2) throw new RuntimeException("Point index must be between 0 and 2!");

        return vertices[index];
    }

    public double getArea(){
        return area;
    }


    public static float calcTriArea(Triangle t){

        float a = Point.distance(t.getPoint(0), t.getPoint(1));
        float b = Point.distance(t.getPoint(1), t.getPoint(2));
        float c = Point.distance(t.getPoint(2), t.getPoint(0));

        double p = (a+b+c)/2;

        double form = p * (p-a) * (p-b) * (p-c);

        return (float)Math.sqrt(form);
    }


}
