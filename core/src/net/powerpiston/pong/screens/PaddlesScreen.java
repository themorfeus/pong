package net.powerpiston.pong.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

import net.powerpiston.pong.MainGame;
import net.powerpiston.pong.entities.Ball;
import net.powerpiston.pong.entities.Entity;
import net.powerpiston.pong.entities.Paddle;

import java.util.ArrayList;

public class PaddlesScreen extends GameScreen implements Screen {

    private Paddle player;
    private Paddle opponent;

    private Paddle top;
    private Paddle bot;

    private Ball ball;

    private Sound insertCoin = Gdx.audio.newSound(Gdx.files.internal("snd/coins.wav"));
    private Sound scoreSnd = Gdx.audio.newSound(Gdx.files.internal("snd/score.wav"));
    private Sound gameover = Gdx.audio.newSound(Gdx.files.internal("snd/gameo.wav"));

    private boolean gameStarted = false;


    public PaddlesScreen(MainGame g){
        super(g);

        entities.clear();
        unbornEntities.clear();
        deadEntities.clear();

        player = new Paddle(g, new Rectangle(50, 0, 35, 250), new Color(.9f,.4f,.4f,1f), true);
        opponent = new Paddle(g, new Rectangle(MainGame.VIRTUAL_WIDTH - 50 - 35, 0, 35, 250), new Color(.4f,.9f,.4f,1f), true);


//

        top = new Paddle(g, new Rectangle(0, 0, 250, 25), new Color(1f,.8f,.8f,1f), false);
        bot = new Paddle(g, new Rectangle(0, MainGame.VIRTUAL_HEIGHT-25, 250, 25), new Color(.8f,1f,.8f,1f), false);

        this.addEntity(top);
        this.addEntity(bot);

        this.addEntity(player);
        this.addEntity(opponent);

        endGame();

        //this.addEntity(new Ball(g, this, 45, .9f));

    }

    public int score;
    private float alpha = 0;

    private long scheduledGameStart = -1;

    @Override
    public void render(float delta) {

        for(Entity e:entities){
            e.update();
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            game.setScreen(new MenuScreen(game));
        }


//        if(Gdx.input.isKeyJustPressed(Input.Keys.Q))score--;
//        if(Gdx.input.isKeyJustPressed(Input.Keys.E))score++;

        if(gameStarted){
            player.setY(game.getGameTouchPoint().getY());
            opponent.moveTowardsY(ball.getLaggedY());

            top.setX(game.getGameTouchPoint().getX());
            bot.setX(ball.getX());
        }else{
            player.setY(MainGame.VIRTUAL_HEIGHT/2);
            opponent.setY(MainGame.VIRTUAL_HEIGHT/2);

            top.setX(MainGame.VIRTUAL_WIDTH/2);
            bot.setX(MainGame.VIRTUAL_WIDTH/2);
        }




        //player.setY(ball.getY());

        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.end();

        shaper.setColor(Color.WHITE);
        shaper.begin(ShapeRenderer.ShapeType.Filled);

        //shaper.rect(game.getGameTouchPoint().getX() - 7, game.getGameTouchPoint().getY() - 7, 14, 14);
//
//        shaper.rect(0, 0, MainGame.VIRTUAL_WIDTH, 25);
//        shaper.rect(0, MainGame.VIRTUAL_HEIGHT-25, MainGame.VIRTUAL_WIDTH, 25);
//
//        shaper.setColor(0.9f, .4f, .4f, 1);
//        shaper.rect(50, paddleY, 35, 250);
//
//        shaper.setColor(.4f, 0.9f, .4f, 1);
//        shaper.rect(MainGame.VIRTUAL_WIDTH - 50 - 35, MainGame.VIRTUAL_HEIGHT -250- paddleY, 35, 250);
//
        shaper.end();

        batch.begin();
        //logo.draw(batch);

        MainGame.getFont(250).setColor(1,1,1,0.5f);
        MainGame.getFont(250).draw(
                batch,
                score + "",
                MainGame.VIRTUAL_WIDTH/2 - MainGame.getFont(250).getMultiLineBounds(score + "").width/2,
                MainGame.VIRTUAL_HEIGHT/2 + MainGame.getFont(250).getLineHeight()/2);
        MainGame.getFont(250).setColor(1,1,1,1);

        MainGame.getFont(50).setColor(1,1,1,0.5f);
        MainGame.getFont(50).draw(
                batch,
                "pong mania",
                MainGame.VIRTUAL_WIDTH/2 - MainGame.getFont(50).getMultiLineBounds("pong mania").width/2,
                MainGame.getFont(50).getLineHeight() + 200);
        MainGame.getFont(50).setColor(1,1,1,1);

        for(Entity e:entities){
            e.render();
        }

        updateEntities();


        if(!gameStarted){


            alpha+=.1f;

            int display = (int)Math.signum(Math.cos(alpha));


            if(display==1 && scheduledGameStart==-1){
                String coin = "Press Q to\ninsert coin";

                MainGame.getFont(150).setColor(1,1,1,1f);
                MainGame.getFont(150).drawMultiLine(
                        batch,
                        coin,
                        0,
                        MainGame.VIRTUAL_HEIGHT / 2 + MainGame.getFont(150).getMultiLineBounds(coin).height / 2,
                        MainGame.VIRTUAL_WIDTH,
                        BitmapFont.HAlignment.CENTER);
                MainGame.getFont(150).setColor(1,1,1,1);

            }

            if(Gdx.input.isKeyJustPressed(Input.Keys.Q)){

                insertCoin.play(.6f);
                score = 0;
                scheduledGameStart = System.currentTimeMillis() + 3000;

            }

            if(System.currentTimeMillis()>=scheduledGameStart && scheduledGameStart!=-1){
                this.addEntity(ball);
                game.calibrateTouch();

                gameStarted = true;
                ball.setMoving(true);
            }
        }
    }

    int gameOverCount = 0;

    public void endGame(){
        gameStarted = false;
        if(gameOverCount!=0)gameover.play(.8f);
        this.removeEntity(ball);
        scheduledGameStart = -1;
        ball = new Ball(game, this);
        gameOverCount++;
    }

    public void addPoint(){
        score++;
        scoreSnd.play(.6f);
        this.removeEntity(ball);
        ball = new Ball(game, this);
        this.addEntity(ball);
        ball.setMoving(true);
    }


    public ArrayList<Entity> getEntities(){
        return entities;
    }

    public void addEntity(Entity e){
        unbornEntities.add(e);
    }

    public void removeEntity(Entity e){
        deadEntities.add(e);
    }

    private void updateEntities(){
        for(Entity e:deadEntities){
            if(entities.contains(e))entities.remove(e);
        }

        for(Entity e:unbornEntities){
            entities.add(e);
        }

        unbornEntities.clear();
        deadEntities.clear();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {}
    @Override
    public void hide() {}
    @Override
    public void pause() {}
    @Override
    public void resume() {}

    @Override
    public void dispose() {

    }


}
