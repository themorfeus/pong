package net.powerpiston.pong.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;

import net.powerpiston.pong.MainGame;

public class MenuScreen implements Screen {

    private MainGame game;

    private Stage stage;
    private SpriteBatch batch;
    private ShapeRenderer shaper;

    private Sprite logo;

    private int optionSelected = 0;

    private String[] options = {"Classic", "4 paddles", "Exit"};

    private Sound select = Gdx.audio.newSound(Gdx.files.internal("snd/select.wav"));
    private Sound enter = Gdx.audio.newSound(Gdx.files.internal("snd/enter.wav"));

    public MenuScreen(MainGame g){
        this.game = g;

        this.stage = g.stage;
        this.batch = g.batch;
        this.shaper = g.shaper;

        logo = new Sprite(new Texture("img/logo.png"));
        logo.setAlpha(.3f);
        logo.setPosition(MainGame.VIRTUAL_WIDTH / 2 - logo.getWidth() / 2, MainGame.VIRTUAL_HEIGHT / 2 - logo.getHeight() / 2);


        int largest = 0;
        for(String s:options){
            if(largest<s.length())largest = s.length();
        }

        for(int i = 0; i<options.length; i++){
            int whitespace = largest - options[i].length() + 1;

            for(int j = 0; j<whitespace; j++){
                options[i]+=" ";
            }
        }
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        logo.draw(batch);

        String title = "Pong mania";

        MainGame.getFont(150).setColor(1,1,1,1f);
        MainGame.getFont(150).drawMultiLine(
                batch,
                title,
                0,
                MainGame.VIRTUAL_HEIGHT / 2 + MainGame.getFont(150).getMultiLineBounds(title).height / 2,
                MainGame.VIRTUAL_WIDTH,
                BitmapFont.HAlignment.CENTER);
        MainGame.getFont(150).setColor(1,1,1,1);



        String opt = "";

        for(int i = 0; i<options.length; i++){
            if(i == optionSelected){
                opt+=">" + options[i].substring(0, options[i].length()-1) + "\n";
            }else
                opt+=options[i] +"\n";
        }

        MainGame.getFont(50).setColor(1,1,1,1f);
        MainGame.getFont(50).drawMultiLine(
                batch,
                opt,
                MainGame.VIRTUAL_WIDTH/2 - MainGame.getFont(50).getMultiLineBounds(opt).width / 2,
                (MainGame.VIRTUAL_HEIGHT / 2 + MainGame.getFont(50).getMultiLineBounds(opt).height / 2) - 200,
                0,
                BitmapFont.HAlignment.LEFT);
        MainGame.getFont(50).setColor(1,1,1,1);


        if(Gdx.input.isKeyJustPressed(Input.Keys.UP) || Gdx.input.isKeyJustPressed(Input.Keys.W)){
            optionSelected--;
            select.play(.5f);
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.DOWN) || Gdx.input.isKeyJustPressed(Input.Keys.S)){
            optionSelected++;
            select.play(.5f);
        }

        if(optionSelected>=options.length)optionSelected = 0;
        if(optionSelected<0)optionSelected = options.length-1;


        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER) || Gdx.input.isKeyJustPressed(Input.Keys.Q)){
            enter.play(.5f);
            handleOption(optionSelected);
        }
    }


    private void handleOption(int op){
        switch(op){
            case 0:
                game.setScreen(new GameScreen(game));
                break;

            case 1:
                game.setScreen(new PaddlesScreen(game));
                break;

            default:
                System.exit(0);
                break;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {}
    @Override
    public void hide() {}
    @Override
    public void pause() {}
    @Override
    public void resume() {}

    @Override
    public void dispose() {

    }


}
